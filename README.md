# Folien von Vorträgen:

## Linux ohne Installation - Kieler Linuxtage 2020 (Kielux)

Linux hat gewonnen und ist heute überall. Deshalb kann man mit Gnu/Linux jederzeit anfangen, oder damit anfangen, es zu lernen, ohne dass man es auf den eigenen Rechner installieren muss.

Dieser Vortrag zeigt die Möglichkeiten auf, die Linux ohne einen Installation auf richtige Hardware bietet, auch wenn man erstmal nur einen Windows-PC, ein Tablet oder Smartphone mit iOS oder Android hat.

Außerdem zeigt der Vortrag, warum das so ist und was die unschlagbare Superkraft von Linux ist, auch in Bereiche vorzudringen, in denen noch nie zuvor ein Betriebssystem gewesen ist.

Was noch eingebaut werden könnte: 
* zu FreeShell - https://in-berlin.de/verein/
* zu Android - Juice-SSH als SSH-Client
* Das Arch-Wiki ist auch super gut!

## F-Droid-Workshop - Linux Presentation Day 2019 in Hannover

Was ist F-Droid und ist das was für mich?

## openSUSE Fun - Kieler Linuxtage 2019 (Kielux)

"Have a Lot of Fun" mit OpenSUSE. Mehr als eine Linux-Distribution 